from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required, permission_required

from users.views import UserView


urlpatterns = [
    url(r'^profile/$', login_required(UserView.as_view()), name='profile'),
    url(r'^(?P<pk>[-\.\w]+)/profile/$', UserView.as_view(), name='profile'),
]
