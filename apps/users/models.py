from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import six, timezone
from django.contrib.auth.validators import ASCIIUsernameValidator, UnicodeUsernameValidator

from django.utils.translation import ugettext_lazy as _

# class User(AbstractUser):
#     username_validator = UnicodeUsernameValidator() if six.PY3 else ASCIIUsernameValidator()
#     username = models.CharField(_('username'), unique=True, primary_key=True, max_length=150,
#         help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
#         validators=[username_validator], error_messages={'unique': _("A user with that username already exists."),},
#     )
#     USERNAME_FIELD = 'username'
