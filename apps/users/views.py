# -*- coding: utf-8 -*-

from django.views.generic import View, TemplateView, DetailView, ListView
from django.shortcuts import get_object_or_404, redirect
from django.http import Http404
from django.contrib.auth.models import User
# from django.contrib.auth import get_user_model
# User = get_user_model()


class UserOwnerMixin(object):
    def get_object(self, *args, **kwargs):
        obj = super(UserOwnerMixin, self).get_object(*args, **kwargs)
        if obj.user != self.request.user:
            raise Http404
        return obj


class UserView(DetailView):
    model = User
    template_name = 'users/profile.html'

    def get_object(self):
        pk = self.kwargs.get('pk', None)
        if pk:
            user = get_object_or_404(User, pk=pk)
        else:
            user = self.request.user
        return user

