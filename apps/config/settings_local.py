from .settings import *

DEBUG = True
SITE_ID = 1

HTML_MINIFY = False
COMPRESS_ENABLED = False

ALLOWED_HOSTS = ['*',]
INTERNAL_IPS = ['127.0.0.1', 'localhost']

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db', 'db.sqlite3'),
    },
}

if DEBUG:
    INSTALLED_APPS += [
        'debug_toolbar',
    ]
    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

    TEMPLATES[0]['OPTIONS']['debug'] = True
    TEMPLATES[0]['APP_DIRS'] = True
    del TEMPLATES[0]['OPTIONS']['loaders']


AUTH_PASSWORD_VALIDATORS = []
