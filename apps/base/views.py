import csv
from io import StringIO, TextIOWrapper
import json
from copy import deepcopy
from string import Formatter
from django.http import HttpResponse
from django.views.generic import View, TemplateView, DetailView, ListView
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse, reverse_lazy


class CSVResponseMixin(object):
    def render_to_response_csv(self, table, filename='file.csv', delimiter=';', quotechar='"', *args, **kwargs):
        '''
            table - двумерный массив
            filename - имя файла
        '''
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename

        writer = csv.writer(response, delimiter=delimiter, quotechar=quotechar)
        for row in table:
            writer.writerow(row)
        return response


class HomeView(TemplateView):
    template_name = 'base/home.html'

    # def dispatch(self, request, *args, **kwargs):
    #     if request.user.is_authenticated():
    #         return redirect('version_status')
    #     return super().dispatch(request, *args, **kwargs)
