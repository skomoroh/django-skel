#### Установка

+ ставим зависисмости
```bash
    easy_install -U pip
    pip install -U pip virtualenv fabric jinja2
```

+ клонируем проект с битбакета
```bash
    mkdir -p django/src
    cd django/src
    git clone git@bitbucket.org:skomoroh/django-skel.git .
    git remote remove origin
    ./setup.sh
```







