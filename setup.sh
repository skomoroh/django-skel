#!/bin/bash
cd "$(dirname "$0")"
mkdir -p static media db log
chmod a+Xwr media db log
# virtualenv -p /usr/bin/python2.7 venv
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate && pip install -U -r requirements.txt
source venv/bin/activate && apps/manage.py migrate
# source venv/bin/activate && apps/manage.py loaddata apps/users/fixtures/user.json
source venv/bin/activate && apps/manage.py createsuperuser --username=root --email='zzz@zzz.zz' --noinput
source venv/bin/activate && apps/manage.py changepassword root
source venv/bin/activate && find apps -name 'data_*.json' -exec apps/manage.py loaddata {} \;

