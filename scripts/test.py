#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

import logging
logger = logging.getLogger('script')
logger.setLevel(logging.DEBUG)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.join(BASE_DIR, 'apps'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
import django
django.setup()

logger.info('start test')

logger.info('stop test')

